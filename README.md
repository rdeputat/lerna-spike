# Lerna spike
Spike for researching how lerna work with building, starting and package publishing in monorepo.

## Pre-requirements
Installed:
* NodeJS LTS
* yarn v1.5+
* nodemon v2+

## How to start

* ### Install local NPM registry
```
$ yarn global add verdaccio
$ verdaccio &

$ npm adduser --registry http://localhost:4873
Username: test
Password: ***
Email: ***
```

## Projects dependecies
### Folder structure
    - applications
        -- fronend
        -- service
    - packages
        -- lib_frontend
        -- lib_backend
        -- lib_core
    
### Dependencies
    frontend
        - lib_frontend
            - lib_core
    
    service
        - lib_service
            - lib_core


## Lerna project settings
### lerna.json
```
{
  "packages": ["packages/*"],   // where are our packages
  "version": "independent",     // each package have independent version number
  "useWorkspaces": true,        // using yarn workspaces
  "npmClient": "yarn"           // using yarn insted of npm
}
```
### package.json
```
{
  "name": "root",
  ...
  "dependencies": {
    "lerna": "^3.19.0"  
  },
  "workspaces": ["packages/*"]  // where are our packages
}
```


## How to use

### Link all dependecies localy
``` 
$ lerna bootstrap 
```
or
```
yarn
```
### Build
```
lerna run build --stream
```

### Watch
```
lerna run watch --parallel
```
for specific project
```
lerna run watch --scope=frontend --include-filtered-dependencies --parallel
```

### Start projects
```
lerna run start --parallel
```

## Publish 
"private": true,
lerna version --conventional-commits

### Conventional Commits
To auto define next version (minor, major or patch) version.  We should use Convention Commit.
By defoult angular convention used https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular#angular-convention.

### Minor versions
1. Create feature branch
1. Change source code of project lib_core
1. Change source code of project lib_frontend
1. Commit&Push with comment ```feat: EDMS-0001 Feature 1```
1. Define new pre versions 
    ```
    lerna version --conventional-commits --conventional-prerelease
    ```
1. Publish prerelese packages
    ```
    lerna publish from-git
    ```
1. Make CI (pre build, tests, ....) 
1. Mearge with develop or master (squash is possible)
1. Define release versions
    ```
    lerna version --conventional-commits --conventional-graduate
    ```
1. Publish release packages
    ```
    lerna publish from-git
    ```
1. Delete feature branch

### Major versions
1. Create feature branch
1. Change source code of project lib_backend
1. Commit&Push with comment 
    ```
    feat: EDMS-0002 Feature 2
    
    description what was done
    ```
1. Change source code of project lib_frontend
1. Commit&Push with comment 
    ```
    feat: EDMS-0002 Feature 2
    
    BREAKING CHANGE: API was changed!!!
    ```

1. Define new pre versions 
    ```
    lerna version --conventional-commits --conventional-prerelease
    ```

1. Publish prerelese packages
    ```
    lerna publish from-git
    ```

1. Make CI (pre build, tests, ....) 
1. Mearge with develop or master (squash is possible)
1. Define release versions
    ```
    lerna version --conventional-commits --conventional-graduate
    ```

1. Publish release packages
    ```
    lerna publish from-git
    ```

1. Delete feature branch


### Useful lerna commands
List all projects
```
lerna ls --all
```

List all projects changed
```
 lerna ls --since --all
```

Run script in package.json for all projects.
```
lerna run <script name>
```

Exec command for all projects.
```
lerna exec <command>
```

Delete node_modules for all projects
```
lerna clean
```


### Problems
* global node_modules - in dev time you can use packages not metioned in package.json
* watch - starts to many node instances (each requare 100-150MB of Memory)
* Change Log: useful information in section just for alfa versions   


