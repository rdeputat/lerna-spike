import packageJson from './package.json';
import {libCoreInfo} from '@package/lib_core';

export const libBackendInfo = `${packageJson.name} v${packageJson.version} dependencies: [ ${libCoreInfo} ]`;
