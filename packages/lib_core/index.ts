import packageJson from './package.json';

export const libCoreInfo = `${packageJson.name} v${packageJson.version}`;
