import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { libFrontendInfo } from '@package/lib_frontend';
import packageJson from '../package.json';

export const frontendInfo: string = `${ packageJson.name } v${ packageJson.version } dependencies: [ ${ libFrontendInfo } ]`;

const App: React.FC = () => {
  const [serviceInfo, setServiceInfo] = useState("");

  useEffect(() => {
      setTimeout(() =>
          fetch("http://localhost:3010")
            .then(async (response) => {
              const txt = await response.text();
              setServiceInfo(txt);
            })
        , 1000);
    }, []
  );

  return (
    <>
      <div>Frontend: { frontendInfo }</div>
      <br/>
      <div>Service: { serviceInfo }</div>
      <div className="App">
        <header className="App-header">
          <img src={ logo } className="App-logo" alt="logo"/>
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    </>
  );
};

export default App;
