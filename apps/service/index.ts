import express from 'express';
import packageJson from './package.json';
import { libBackendInfo } from '@package/lib_backend';
import cors from 'cors';

export const serviceInfo = `${packageJson.name} v${packageJson.version} dependencies: [ ${libBackendInfo} ]`;

const app = express();
const port = 3010;
app.use(cors())
app.get('/', (req, res) => {
  res.send(serviceInfo);
});
app.listen(port, err => {
  if (err) {
    return console.error(err);
  }
  return console.log(`server is listening on ${port}`);
});
